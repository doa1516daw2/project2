/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuari;

import data.BDAccessor;
import lluitador.*;
import static data.BDAccessor.obtenirConnexio;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import Equip.*;

/**
 *
 * @author dgs
 */
public class UsuariDAOImp implements UsuariDAO {
    

    private BDAccessor bd = new BDAccessor();

    @Override
    public void crearLluitador(Lluitador lluitador,int id) throws SQLException {
        
      try(Connection conn = bd.obtenirConnexio()){
            PreparedStatement sentencia = conn.prepareStatement(""
                    + "insert into lluitador(nom,punt_atac,punt_defensa,medi,habilitat,raca,victoria,derrota,empat,preparat,id_user,id_equip)"
                    + "values(?,?,?,?,?,?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            System.out.println(lluitador.getNom());
            System.out.println(sentencia);
            sentencia.setString(1, lluitador.getNom());
            sentencia.setInt(2, lluitador.getpAtac());
            sentencia.setInt(3, lluitador.getpDefensa());
            sentencia.setString(4, lluitador.getMedi());
            sentencia.setString(5, lluitador.gethEspecials());
            sentencia.setString(6, lluitador.getRaca());
            sentencia.setInt(7, lluitador.getCombatGuanyat());
            sentencia.setInt(8, lluitador.getCombatPerdut());
            sentencia.setInt(9, lluitador.getCombatEmpatat());
            sentencia.setBoolean(10, lluitador.isReady());
            // TODO: cambiar aquestes dos
            sentencia.setInt(11, id);
            sentencia.setInt(12, 0);
            
            sentencia.executeUpdate();
//            conn.commit();
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
//        System.out.println(lluitador);
    }
    
    @Override
    public void llistarLluitadors(int id) throws SQLException {
        PreparedStatement pstmt = null;

        try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT * FROM lluitador where id_user=?";            
            pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setInt(1, id);
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {                   
                    System.out.println("nom: "+ resultat.getString(2)+"// punts atac:"+resultat.getInt(3)+
                            "// punts defensa:"+resultat.getInt(4)+"// medi:"+resultat.getString(5)+"//habilitat:"+resultat.getString(6)+"");
                           

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    @Override
    public void modificarLluitador(String nom, String lema, int id_user) throws SQLException {

        PreparedStatement pstmt = null;
            int integrans=0;
          int equip=idEquip(lema,id_user);
      try (Connection connexio = bd.obtenirConnexio()) {
       
            pstmt = connexio.prepareStatement(
                    "UPDATE lluitador set id_equip = ? where id_user= ?  and nom=?");
           
            pstmt.setInt(1, equip);
            pstmt.setInt(2, id_user);
            pstmt.setString(3, nom);
            pstmt.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();
        }
      try (Connection connexio = bd.obtenirConnexio()) {
       
            pstmt = connexio.prepareStatement(
                    "select lluitadors from  equip  where id_user= ?");
            pstmt.setInt(1, id_user);
           
            pstmt.execute();
                try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {                   
                integrans= resultat.getInt(1);

                }
            }
            

        } catch (Exception e) {
            e.printStackTrace();
        }
      integrans++;//summem un als integrans del equip
        try (Connection connexio = bd.obtenirConnexio()) {
       
            pstmt = connexio.prepareStatement(
                    "UPDATE equip set lluitadors = ? where id_equip= ? ");
           
            pstmt.setInt(1, integrans);
            pstmt.setInt(2, equip);
          
            pstmt.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();
        }
      
      
     }
    
    @Override
    public void esborrarLluitador(String nom,int id) throws SQLException {


        PreparedStatement pstmt = null;
        try (Connection connexio = bd.obtenirConnexio()) {

            pstmt = connexio.prepareStatement(
                    "DELETE FROM lluitador WHERE nom= ? and id_user=?");
            pstmt.setString(1, nom);
            pstmt.setInt(2, id);
            
            pstmt.executeUpdate();
//            connexio.commit();
        } catch (Exception e) {
            throw new SQLException();
        }
    }

    @Override
    public Lluitador consultarLluitador(String nom) throws SQLException {
      PreparedStatement pstmt = null;
//        List<Lluitador> llista = new ArrayList<>(50);
        try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT * FROM lluitador where nom = ?";
            pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setString(1, nom);
            
            try (ResultSet resultat = pstmt.executeQuery()) {            
                
                 while (resultat.next()) {
//                System.out.println("id lluitador"+ resultat.getInt(1));
               Lluitador ll=new Lluitador(
                                resultat.getInt(1),
                                resultat.getString(2),
                                resultat.getInt(3),
                                resultat.getInt(4),
                                resultat.getString(5),
                                resultat.getString(6),
                                resultat.getInt(9)
                           
                                

                        );
                
                return ll;
                 }
               
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
      return null;
    }

    @Override
    public Lluitador buscarRival(String nom) throws SQLException {
        PreparedStatement pstmt = null;
//        List<Lluitador> llista = new ArrayList<>(50);
        try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "select * from lluitador where id_user NOT IN "
                    + "(select id_user from usuari where nom = ?)";
            pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setString(1, nom);
            
            try (ResultSet resultat = pstmt.executeQuery()) {            
                
                 while (resultat.next()) {
                  Lluitador ll=new Lluitador(
                                resultat.getInt(1),
                                resultat.getString(2),
                                resultat.getInt(3),
                                resultat.getInt(4),
                                resultat.getString(5),
                                resultat.getString(6),
                                resultat.getInt(9)
                           
                                

                        );
                
                return ll;
                 }
               
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
      return null;
    }
 	@Override
    public void crearEquip(Equip equip,int id) throws SQLException{
       try(Connection conn = bd.obtenirConnexio()){
            PreparedStatement sentencia = conn.prepareStatement(""
                    + "insert into equip(lema,lluitadors,victories,derrotes,empats,id_user)"
                    + "values(?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            sentencia.setString(1, equip.getLema());
            sentencia.setInt(2, equip.getNumLluitadors());
            sentencia.setInt(3, equip.getVictories());
            sentencia.setInt(4, equip.getDerrotes());
            sentencia.setInt(5, equip.getEmpats());
            sentencia.setInt(6, id);

            
            sentencia.executeUpdate();
//            conn.commit();
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        System.out.println(equip);
    }

    @Override
    public void llistarEquips(int id) throws SQLException {
          PreparedStatement pstmt = null;
//        List<Lluitador> llista = new ArrayList<>(50);
        try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT * FROM equip where id_user=?";            
            pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setInt(1, id);
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {                   
                    System.out.println("Lema: "+ resultat.getString(2)+"// Integrans: "+resultat.getInt(3));


                }
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }

    @Override
    public void esborrarEquip(String lema, int id) throws SQLException {
        PreparedStatement pstmt = null;
        int id_equip=idEquip(lema,id);
                
          try (Connection connexio = bd.obtenirConnexio()) {
       
            pstmt = connexio.prepareStatement(
                    "UPDATE lluitador set id_equip = ? where id_user= ?  and id_equip=?");
           
            pstmt.setInt(1, 0);
            pstmt.setInt(2, id);
            pstmt.setInt(3, id_equip);
            pstmt.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();
        }        
           try (Connection connexio = bd.obtenirConnexio()) {

            pstmt = connexio.prepareStatement(
                    "DELETE FROM equip WHERE id_equip= ? and id_user=?");
            pstmt.setInt(1, id_equip);
            pstmt.setInt(2, id);
            
            pstmt.executeUpdate();

        } catch (Exception e) {
            throw new SQLException();
        }
    }

    @Override
    public ArrayList<Lluitador> consultarEquip(String lema, int id) throws SQLException{
       int id_equip=idEquip(lema,id);
        System.out.println("id equip "+id_equip);
       
         PreparedStatement pstmt = null;
    ArrayList<Lluitador> equip = new ArrayList<Lluitador>();
        try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT * FROM lluitador where id_user=? and id_equip=?";            
            pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setInt(1, id);
            pstmt.setInt(2, id_equip);
            
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {      
//                     System.out.println("id lluitador"+ resultat.getInt(1));
                     Lluitador ll=new Lluitador(
                                resultat.getInt(1),
                                resultat.getString(2),
                                resultat.getInt(3),
                                resultat.getInt(4),
                                resultat.getString(5),
                                resultat.getString(6),
                                resultat.getInt(9)
                           
                                

                        );
                       equip.add(ll);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }

      
       return equip;
       
       
        

    }

    @Override
    public ArrayList<Lluitador> buscarEquipRival(String lema,int id) throws SQLException {
        int id_equip=idEquip(lema,id);
//        System.out.println("id equip "+id_equip);
       
         PreparedStatement pstmt = null;
    ArrayList<Lluitador> equip = new ArrayList<Lluitador>();
        try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT * FROM lluitador where id_user!=? and id_equip!=?";            
            pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setInt(1, id);
            pstmt.setInt(2, id_equip);
            
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {     
//                    System.out.println("id lluitador"+ resultat.getInt(1));
                        Lluitador ll=new Lluitador(
                                resultat.getInt(1),
                                resultat.getString(2),
                                resultat.getInt(3),
                                resultat.getInt(4),
                                resultat.getString(5),
                                resultat.getString(6),
                                resultat.getInt(9)
                      );

                       equip.add(ll);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }

      
       return equip;
    }
    
    @Override
    public void actualitzarLluitador(int v, int d, int em, int id) throws SQLException {
        int iderrotes = 0;
        int iempats = 0;
        int iv=0; 
        int vVic=0;
        int vderrotes=0;
        int vemp=0;
        //////////////////////////////
         Connection con = null;
          bd  = new BDAccessor();
        try {
            con= bd.obtenirConnexio();
        } catch (IOException ex) {
           
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
         con.setAutoCommit(false);
         
          try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT victoria FROM lluitador where id_lluitador=? ";            
              PreparedStatement pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setInt(1, id);
         
            
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {     
                   vVic = resultat.getInt(1);
                     
                     
                }
            }
        } catch (IOException ex) {
             con.rollback();
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
          try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT derrota FROM lluitador where id_lluitador=?";            
              PreparedStatement pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setInt(1, id);
         
            
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {     
                     vderrotes = resultat.getInt(1);
                     
                     
                }
            }
        } catch (IOException ex) {
             con.rollback();
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
          try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT empat FROM lluitador where id_lluitador=?";            
              PreparedStatement pstmt = connexio.prepareStatement(cadenaSQL);
                pstmt.setInt(1, id);
         
            
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {     
                   vemp = resultat.getInt(1);
                     
                     
                }
            }
        } catch (IOException ex) {
             con.rollback();
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
          if(v==1){
             iv=vVic+1;
            iderrotes=vderrotes;
             iempats=vemp;
        }else if(d==1){
              iv=vVic;
             iderrotes=vderrotes+1;
              iempats+=vemp;
        }else{
            iv=vVic;
            iderrotes=vderrotes;
            iempats+=vemp+1;
        }
         
         
         
        try (Connection connexio = bd.obtenirConnexio()) {
       
            PreparedStatement pstmt = connexio.prepareStatement(
                    "UPDATE lluitador set victoria = ?, derrota=?, empat=? where id_lluitador= ?");
           
            pstmt.setInt(1, iv);
            pstmt.setInt(2, iderrotes);
            pstmt.setInt(3, iempats);
            pstmt.setInt(4, id);
            
            pstmt.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();
            con.rollback();
        }
        
     
        con.commit();
       

    }

    @Override
    public void actualitzarEquip(int v, int d, int em, int id) throws SQLException {
        int iderrotes = 0;
        int iempats = 0;
        int iv=0; 
        int vVic=0;
        int vderrotes=0;
        int vemp=0;
          try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT victories FROM equip where id_equip=?";            
              PreparedStatement pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setInt(1, id);
         
            
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {     
                   vVic = resultat.getInt(1);
                     
                     
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
          try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT derrotes FROM equip where id_equip=?";            
              PreparedStatement pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setInt(1, id);
         
            
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {     
                     vderrotes = resultat.getInt(1);
                     
                     
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
          try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT empats FROM equip where id_equip=?";            
              PreparedStatement pstmt = connexio.prepareStatement(cadenaSQL);
                pstmt.setInt(1, id);
         
            
            try (ResultSet resultat = pstmt.executeQuery()) {
                while (resultat.next()) {     
                   vemp = resultat.getInt(1);
                     
                     
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
          if(v==1){
             iv=vVic+1;
            iderrotes=vderrotes;
             iempats=vemp;
        }else if(d==1){
              iv=vVic;
             iderrotes=vderrotes+1;
              iempats+=vemp;
        }else{
            iv=vVic;
            iderrotes=vderrotes;
            iempats+=vemp+1;
        }
         
         
         
        try (Connection connexio = bd.obtenirConnexio()) {
       
            PreparedStatement pstmt = connexio.prepareStatement(
                    "UPDATE equip set victories = ?, derrotes=?, empats=? where id_equip=?");
           
            pstmt.setInt(1, iv);
            pstmt.setInt(2, iderrotes);
            pstmt.setInt(3, iempats);
            pstmt.setInt(4, id);
            
            pstmt.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();
        }
    
    }
    
    public int idEquip(String Lema, int id) throws SQLException {
         PreparedStatement pstmt2 = null;
        int id_equip = 0;

        try (Connection connexio = bd.obtenirConnexio()) {
       
            pstmt2 = connexio.prepareStatement(
                    "select id_equip from equip where lema=?");
           
            pstmt2.setString(1, Lema);   
          
              try (ResultSet resultat = pstmt2.executeQuery()) {
                while (resultat.next()) {                   
                        
                           id_equip=resultat.getInt(1);
//                    System.out.println("id equip "+ id_equip);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return id_equip;
    }


    
}
