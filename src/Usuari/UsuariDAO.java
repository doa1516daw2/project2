/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuari;

import java.io.IOException;
import java.sql.SQLException;
import lluitador.Lluitador;
import java.util.List;
import lluitador.Lluitador;
import Equip.*;
import java.util.ArrayList;
/**
 *
 * @author dgs
 */
public interface UsuariDAO {
    

    public void crearLluitador(Lluitador lluitador, int id) throws SQLException;
    public void llistarLluitadors(int id) throws SQLException;
    public void llistarEquips(int id) throws SQLException;
    public void esborrarLluitador(String nom, int id) throws SQLException;
    public void esborrarEquip(String lema, int id) throws SQLException;
    public void modificarLluitador(String nom, String lema, int id_user) throws SQLException;    
    public Lluitador consultarLluitador(String nom) throws SQLException;    
    public ArrayList<Lluitador>  consultarEquip(String lema, int id) throws SQLException;    
    public Lluitador buscarRival(String nom) throws SQLException;
    public ArrayList<Lluitador> buscarEquipRival(String lema, int id) throws SQLException;
    public void crearEquip(Equip equip, int id) throws SQLException;
    public void actualitzarLluitador(int v, int d, int e,int id)throws SQLException;
    public void actualitzarEquip(int v, int d, int e,int id)throws SQLException;
    }
