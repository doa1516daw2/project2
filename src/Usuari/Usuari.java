/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuari;

/**
 *
 * @author gerard
 */
public interface Usuari {
    
    public String getLema();

    public void setLema(String lema);
   
    public void setPassword(String password);

    public String getPassword();

    public void setUserName(String userName);

    public String getUserName();
    
}
