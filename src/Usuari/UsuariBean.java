/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuari;

/**
 *
 * @author dgs
 */
public class UsuariBean extends Entity implements Usuari{
     private String lema;

    public UsuariBean(String lema, String userName, String password) {
        super(userName, password);
        this.lema = lema;
    }
    public UsuariBean() {
    }    

    public String getLema() {
        return lema;
    }

    public void setLema(String lema) {
        this.lema = lema;
    }
   
    @Override
    public void setPassword(String password) {
        super.setPassword(password); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getPassword() {
        return super.getPassword(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setUserName(String userName) {
        super.setUserName(userName); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getUserName() {
        return super.getUserName(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
