/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Combat;

import Factory.DAOFactory;
import Usuari.UsuariDAO;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import lluitador.Lluitador;

/**
 *
 * @author dgs
 */
public class Combat {

    public ArrayList<String> medi = new ArrayList<String>();
     public int cCombats = 1;
     public  int cGuanyats1 = 0;
     public  int cGuanyats2 = 0;
     public  int empats = 0;
     public int c = 0;
     public int estrelles1=0;
     public int estrelles2=0;

    public int combatEquip(ArrayList<Lluitador> equip1, ArrayList<Lluitador> equip2) throws SQLException {
        UsuariDAO dao5 = DAOFactory.crearLluitadorDAOJDBC();
        int e1 = equip1.size();
        int e2 = equip2.size();
        //calcul estrelles
        for (int i = 0; i < e1; i++){
            estrelles1+=(equip1.get(i)).getpAtac();
        }
        for (int i = 0; i < e2; i++){
            estrelles2+=(equip2.get(i)).getpAtac();
        }
        if(estrelles1<10){
            estrelles1=0;
        }else if(estrelles1<20){
            estrelles1=1;
        } else if(estrelles1<30){
            estrelles1=2;
        }else if(estrelles1<40){
            estrelles1=3;
        }else if(estrelles1<50){
            estrelles1=4;
        }else{
            estrelles1=5;
        }
        if(estrelles2<10){
            estrelles2=0;
        }else if(estrelles2<20){
            estrelles2=1;
        } else if(estrelles2<30){
            estrelles2=2;
        }else if(estrelles2<40){
            estrelles2=3;
        }else if(estrelles2<50){
            estrelles2=4;
        }else{
            estrelles2=5;
        }
        ///////////////////////
            
        System.out.println("Equip1 " +estrelles1+" Estrelles");
        System.out.println("Equip2 " +estrelles2+" Estrelles");
        //per saber el nombre de combats
        if (e1 >= e2) {
            c = e2;

        } else {
            c = e1;

        }


        System.out.println("S'inicia el combat perequips!!!");
        for (int i = 0; i < c; i++) {
            System.out.println("S'inicia el combat!! " + cCombats);
            System.out.println("Lluitador " + (equip1.get(i)).getNom() + " Atac:" + (equip1.get(i)).getpAtac() + " Def: " + (equip1.get(i)).getpDefensa()
                    + " vs " + (equip2.get(i)).getNom() + " Atac:" + (equip2.get(i)).getpAtac() + " Def: " + (equip2.get(i)).getpDefensa());
            int res = combat(equip1.get(i), equip2.get(i));

            if (res == 1) {
                cGuanyats1++;
                if (cGuanyats1 == 4) {
                    System.out.println("Ha guanyat el equip1");
                    i = c;
                    dao5.actualitzarEquip(1, 0, 0, equip1.get(1).getId_equip());
                    dao5.actualitzarEquip(0, 1,0 , equip2.get(1).getId_equip());
                 
                    return 1;

                }
            } else if (res == 4) {
                cGuanyats2++;
                if (cGuanyats2 == 3) {
                    System.out.println("Ha guanyat el equip2");
                    i = c;
                    dao5.actualitzarEquip(0, 1, 0, equip1.get(1).getId_equip());
                    dao5.actualitzarEquip(1, 0,0 , equip2.get(1).getId_equip());
                    return 2;
                }
            } else {
                    dao5.actualitzarEquip(0,0,1,equip1.get(1).getId_equip());
                    dao5.actualitzarEquip(0,0,1,equip2.get(1).getId_equip());
                empats++;

            }
            cCombats++;
            System.out.println("Victories equip1: " + cGuanyats1);
            System.out.println("Victories equip2: " + cGuanyats2);
            System.out.println("Empats: " + empats);
            System.out.println("----------------------------------------------");
        }

        return 4;
    }

    public int combat(Lluitador l1, Lluitador l2) throws SQLException {
        System.out.println("FIGHT!!!!!!");
        System.out.println(l1.gethEspecials());
        String m1 = "aigua";
        String m2 = "terra";
        String m3 = "aire";
        String m4 = "foc";

        medi.add(m1);
        medi.add(m2);
        medi.add(m3);
        medi.add(m4);

        int a = medialeatori();

        medi.get(a);
        System.out.println("el medi es " + medi.get(a));
        if (medi.get(a).equals(l1.getMedi())) {
            System.out.println("El medi  d'en " + l1.getNom() + " coincideix!, Bonus ATT+1 DEF+1");
            l1.setpAtac(l1.getpAtac() + 1);
            l1.setpDefensa(l1.getpDefensa() + 1);

        }
        if (medi.get(a).equals(l2.getMedi())) {
            System.out.println("El medi  d'en " + l2.getNom() + " coincideix!, Bonus ATT+1 DEF+1");
            l1.setpAtac(l2.getpAtac() + 1);
            l1.setpDefensa(l2.getpDefensa() + 1);

        }

        if ("Atac kaito".equals(l1.gethEspecials())) {
            l1.setpAtac(kaito(l1.getpAtac()));
            System.out.println("atac d'en kaito " + l1.getNom() + " activat. atac:  " + l1.getpAtac());
        } else if ("Atac rapid".equals(l1.gethEspecials()) && !"Atac rapid".equals(l2.gethEspecials())) {
            System.out.println("Atac rapid " + l1.getNom() + " activat");
            if (l1.getpAtac() >= l2.getpDefensa()) {

                return resultat(1, l1,l2);
            } else {
                System.out.println("no ha servit de res!");
            }

        }
        if ("Atac kaito".equals(l2.gethEspecials())) {
            l2.setpAtac(kaito(l2.getpAtac()));
            System.out.println("atac d'en kaito " + l2.getNom() + " activat. atac:  " + l2.getpAtac());

        } else if (l2.gethEspecials() == "Atac rapid" && !"Atac rapid".equals(l1.gethEspecials())) {
            System.out.println("Atac rapid " + l1.getNom() + " activat!");
            if (l2.getpAtac() >= l1.getpDefensa()) {
                return resultat(2, l1,l2);
            } else {
                System.out.println("no ha servit de res!");
            }

        }

        if (l1.getpAtac() >= l2.getpDefensa() && l2.getpAtac() >= l1.getpDefensa()) {
            return resultat(3, l1,l2);
        } else if (l1.getpAtac() >= l2.getpDefensa() && l2.getpAtac() <= l1.getpDefensa()) {
            return resultat(1, l1,l2);
        } else if (l1.getpAtac() <= l2.getpDefensa() && l2.getpAtac() >= l1.getpDefensa()) {
            return resultat(2, l1,l2);
        } else {
            return resultat(3, l1,l2);

        }

    }

    public int resultat(int res, Lluitador l1,Lluitador l2) throws SQLException {
         UsuariDAO dao4 = DAOFactory.crearLluitadorDAOJDBC();
           
        if (res == 1) {
            System.out.println("Ha guanyat el lluitador " + l1.getNom());
//            System.out.println("id l1"+l1.getId());
             dao4.actualitzarLluitador(1, 0, 0,l1.getId());
             dao4.actualitzarLluitador(0, 1, 0,l2.getId());
            return res;
        } else if (res == 2) {
            System.out.println("Ha guanyat el lluitador " + l2.getNom());
               dao4.actualitzarLluitador(0, 1, 0,l1.getId());
             dao4.actualitzarLluitador(1, 0, 0,l2.getId());
            return res;

        } else {
            System.out.println("Empat!");
                dao4.actualitzarLluitador(0, 0, 1,l1.getId());
             dao4.actualitzarLluitador(1, 0, 1,l2.getId());
            return 3;
        }

    }

    public int kaito(int atac) {

        int numAleatorio = (int) Math.floor(Math.random() * (1 - 4) + 4);

        atac += numAleatorio;

        return atac;
    }

    public int medialeatori() {

        int numAleatorio = (int) Math.floor(Math.random() * (0 - 4) + 4);

        return numAleatorio;
    }

}
