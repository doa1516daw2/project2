/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lluitador;
import java.lang.Comparable;
import java.util.ArrayList;

/**
 *
 * @author dani
 */
public class Lluitador implements Comparable<Lluitador>{
    private int id;
    private String nom;
    private int pAtac;
    private int pDefensa;
    private String raca;
    private String medi;
    private String hEspecials;
    private boolean ready;
    private int combatGuanyat=0;
    private int combatPerdut=0;
    private int combatEmpatat=0;
    private int id_equip=0;

    
    public Lluitador() {
    
    }

    public Lluitador(String nom, int combatGuanyat, int combatPerdut, int combatEmpatat) {
        this.nom = nom;
        this.combatGuanyat = combatGuanyat;
        this.combatPerdut = combatPerdut;
        this.combatEmpatat = combatEmpatat;
    }
    
    

    public Lluitador(String nom, int pAtac, int pDefensa, String medi, String hEspecials, String raca, boolean ready) {
       
        this.nom = nom;
        this.pAtac = pAtac;
        this.pDefensa = pDefensa;
        this.medi = medi;
        this.hEspecials = hEspecials;
        this.raca = raca;      
        this.ready = ready;
    
    }

    public Lluitador(int aInt, String string, int aInt0, int aInt1, String string0, String string1, int aInt2){
         this.id= aInt;
        this.nom = string;
        this.pAtac = aInt0;
        this.pDefensa = aInt1;
        this.medi = string0;
        this.hEspecials = string1;       
        this.id_equip=aInt2;
    }
    
    

   

    public int getId_equip() {
        return id_equip;
    }

    public void setId_equip(int id_equip) {
        this.id_equip = id_equip;
    }
   
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
   
  
    

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    public int getpAtac() {
        return pAtac;
    }

    public void setpAtac(int pAtac) {
        this.pAtac = pAtac;
    }

    
    public int getpDefensa() {
        return pDefensa;
    }

    public void setpDefensa(int pDefensa) {
        this.pDefensa = pDefensa;
    }

    
    public String getMedi() {
        return medi;
    }

    public void setMedi(String medi) {
        this.medi = medi;
    }

    
    public String gethEspecials() {
        return hEspecials;
    }

    public void sethEspecials(String hEspecials) {
        this.hEspecials = hEspecials;
    }

    
    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }
    
    
    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    
    public int getCombatGuanyat() {
        return combatGuanyat;
    }

    public void setCombatGuanyat(int combatGuanyat) {
        this.combatGuanyat = combatGuanyat;
    }

    
    public int getCombatPerdut() {
        return combatPerdut;
    }

    public void setCombatPerdut(int combatPerdut) {
        this.combatPerdut = combatPerdut;
    }

    
    public int getCombatEmpatat() {
        return combatEmpatat;
    }

    public void setCombatEmpatat(int combatEmpatat) {
        this.combatEmpatat = combatEmpatat;
    }
    
    

    @Override
    public String toString() {
        return "LluitadorBean{" + "nom=" + nom + ", pAtac=" + pAtac + ", pDefensa=" + pDefensa + ", raca=" + raca + ", medi=" + medi + ", hEspecials=" + hEspecials + ", ready=" + ready + ", combatGuanyat=" + combatGuanyat + ", combatPerdut=" + combatPerdut + ", combatEmpatat=" + combatEmpatat + '}';
    }      

    @Override
    public int compareTo(Lluitador o){
        int compareVictories = ((Lluitador) o).getCombatGuanyat(); 
		
		//ascending order
		return this.combatGuanyat - compareVictories;
    }
}

