/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package administrador;

import Usuari.Entity;

/**
 *
 * @author gerard
 */
public class AdministradorBean extends Entity implements Administrador{
    
    private String rol;

    public AdministradorBean() {
    }

    public AdministradorBean(String rol) {
        this.rol = rol;
    }

    @Override
    public String getRol() {
        return rol;
    }

    @Override
    public void setRol(String rol) {
        this.rol = rol;
    }
    
    
    
}
