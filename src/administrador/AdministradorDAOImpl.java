/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package administrador;

import Usuari.Entity;
import Usuari.Usuari;
import Usuari.UsuariDAOImp;
import data.BDAccessor;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lluitador.Lluitador;

/**
 *
 * @author gerard
 */
public class AdministradorDAOImpl implements AdministradorDAO {
    
    BDAccessor bd = new BDAccessor();

    /**
     * 
     * @param usuari
     * @throws SQLException 
     */
    
    @Override
    public void crearUsuari(Usuari usuari) throws SQLException {
        
        try(Connection conn = bd.obtenirConnexio()){
            
            PreparedStatement sentencia = conn.prepareStatement(""
                    + "insert into usuari(nom,contrasenya,lema)"
                    + "values(?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            
            
           sentencia.setString(1, ((Entity)usuari).getUserName());
           sentencia.setString(2, ((Entity)usuari).getPassword());
           sentencia.setString(3, usuari.getLema());
            
            sentencia.executeUpdate();
//            conn.commit();
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        System.out.println(usuari);
    }
    
    /**
     * 
     * @param nom
     * @throws SQLException 
     */
    @Override
    public void esborrarUsuari(String nom) throws SQLException {

        PreparedStatement pstmt = null;
        try (Connection connexio = bd.obtenirConnexio()) {

            pstmt = connexio.prepareStatement(
                    "DELETE FROM usuari WHERE nom = ?");
            pstmt.setString(1, nom);
            pstmt.executeUpdate();
//            connexio.commit();
        } catch (Exception e) {
            throw new SQLException();
        }
    
    }
    
    /**
     * 
     * @param nom
     * @param codi
     * @throws SQLException 
     */
    @Override
    public void modificarUsuari(String nom, int codi) throws SQLException {
        PreparedStatement pstmt = null;
        try (Connection connexio = bd.obtenirConnexio()) {
       
            pstmt = connexio.prepareStatement(
                    "UPDATE usuari set nom= ? where id_user= ? ");
           
            pstmt.setString(1, nom);
            pstmt.setInt(2, codi);
            pstmt.executeUpdate();
            //connexio.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
     }

    @Override
    public int validarUsuari(String nom, String pass) throws SQLException {
         PreparedStatement pstmt = null;
        try (Connection connexio = bd.obtenirConnexio()) {
            String cadenaSQL = "SELECT id_user FROM usuari where nom = ? and contrasenya=?";
            pstmt = connexio.prepareStatement(cadenaSQL);
            pstmt.setString(1, nom);
            pstmt.setString(2, pass);
            
            try (ResultSet resultat = pstmt.executeQuery()) {            
                
                 while (resultat.next()){             
             
                 int id=resultat.getInt(1);
                   
                return id;
                 }
               
            }
        } catch (IOException ex) {
            Logger.getLogger(UsuariDAOImp.class.getName()).log(Level.SEVERE, null, ex);
        }
      return 0;
        
        
       
    }
    
}
