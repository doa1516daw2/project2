/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package administrador;

import Usuari.Usuari;
import java.sql.SQLException;

/**
 *
 * @author gerard
 */
public interface AdministradorDAO {
    
    /**
     * 
     * @param usuari
     * @throws SQLException 
     */
    public void crearUsuari(Usuari usuari) throws SQLException;
    
    /**
     * 
     * @param nom
     * @throws SQLException 
     */
    public void esborrarUsuari(String nom) throws SQLException;
    
    /**
     * 
     * @param nom
     * @param codi
     * @throws SQLException 
     */
    public void modificarUsuari(String nom, int codi) throws SQLException;
    
    public int validarUsuari(String nom, String pass)throws SQLException;
}
