/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Equip;

/**
 *
 * @author dgs
 */
public class Equip {
    private String lema;
    private int numLluitadors;
    private int victories=0;
    private int derrotes=0;
    private int empats=0;

    public Equip() {
        
    }
    
    public Equip(String lema, int numLluitadors) {
        this.lema = lema;
        this.numLluitadors = numLluitadors;
    }

    public Equip(String lema) {
        this.lema = lema;
    }
    

    public String getLema() {
        return lema;
    }

    public void setLema(String lema) {
        this.lema = lema;
    }

    public int getNumLluitadors() {
        return numLluitadors;
    }

    public void setNumLluitadors(int numLluitadors) {
        this.numLluitadors = numLluitadors;
    }

    public int getVictories() {
        return victories;
    }

    public void setVictories(int victories) {
        this.victories = victories;
    }

    public int getDerrotes() {
        return derrotes;
    }

    public void setDerrotes(int derrotes) {
        this.derrotes = derrotes;
    }

    public int getEmpats() {
        return empats;
    }

    public void setEmpats(int empats) {
        this.empats = empats;
    }

     
      
}
