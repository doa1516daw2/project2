package Factory;

import Usuari.UsuariDAO;
import Usuari.UsuariDAOImp;
import administrador.AdministradorDAO;
import administrador.AdministradorDAOImpl;

/**
 * Classe factoria d'objectes DAO.
 * Podem invocar a travñes d'un mètode estàtic la instanciació
 * d'un objecte que correspon a un objecte EmpleatDAO.
 * Aquest patró ens proporciona un contracte per crear objectes sense
 * especificar la  classe concreta a la que corresponen.
 * 
 * @author Sergi Grau.
 * @version 1.0 28.02.2013
 */

public class DAOFactory {

    /**
     * 
     * @return 
     */
    public static UsuariDAO crearLluitadorDAOJDBC(){
        return new UsuariDAOImp();
    }
        
    /**
     * 
     * @return 
     */
    public static AdministradorDAO crearUsuariDAOJDBC(){
        return new AdministradorDAOImpl();
    }

	public static UsuariDAO crearEquipDAOJDBC(){
            return new UsuariDAOImp();
        }

}
